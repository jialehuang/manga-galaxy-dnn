import os
import tensorflow as tf
import argparse
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt

BATCH_SIZE = 2000
LOGDIR = 'train_model'
CHECKPOINT_EVERY = 50
NUM_STEPS = int(3e3)
LEARNING_RATE = 1e-3
KEEP_PROB = 0.8
L2_REG = 0
decay_steps = 1500
decay_rate = 0.5

from astropy.io import fits
import random
import glob
import gc

class DataReader(object):
    def __init__(self, idx, sequential=False):
        self.load(idx)

    def load(self, idx):
        xs = []
        ys = []
        zs = []

        self.train_batch_pointer = 0
        self.test_batch_pointer = 0

        total = 0
        count01 = count13 = count35 = count50 = count_up = 0

        #Obtain number of galaxies in each z category
        for file in glob.glob("MaNGA_DR15_" + str(idx) + "/fits/*.fits"):
            hdulnew = fits.open(file)
            hdu_hd = hdulnew[0].header
            z = hdu_hd['Z']
            lgm = hdu_hd['STELLAR_MASS']
            if z < 0.01:
                xs.append(file)
                ys.append(lgm/10.)
                zs.append(z)
                count01 += 1
            elif z > 0.01 and z < 0.03:
                xs.append(file)
                ys.append(lgm/10.)
                zs.append(z)
                count13 += 1
            elif z > 0.03 and z < 0.05:
                xs.append(file)
                ys.append(lgm/10.)
                zs.append(z)
                count35 += 1
            elif z > 0.05 and z < 0.10:
                xs.append(file)
                ys.append(lgm/10.)
                zs.append(z)
                count50 += 1
            else:
                xs.append(file)
                ys.append(lgm/10.)
                zs.append(z)
                count_up += 1
            total += 1
            hdulnew.close()

        print('< 0.01: ' + str(count01))            
        print('> 0.01 and < 0.03: ' + str(count13))
        print('> 0.03 and < 0.05: ' + str(count35))
        print('> 0.05 and < 0.10: ' + str(count50))
        print('> 0.1: ' + str(count_up))
        print('Total data: ' + str(total))

        self.num_images = len(xs)

        # Randomly shuffle the dataset and partition into training and testing sets
        c = list(zip(xs, ys, zs))
        random.shuffle(c)
        xs, ys, zs = zip(*c)

        self.train_xs = xs[:int(len(xs) * 0.8)]
        self.train_ys = ys[:int(len(xs) * 0.8)]
        self.train_zs = zs[:int(len(xs) * 0.8)]

        self.test_xs = xs[-int(len(xs) * 0.2):]
        self.test_ys = ys[-int(len(xs) * 0.2):]
        self.test_zs = zs[-int(len(xs) * 0.2):]

        self.num_train_images = len(self.train_xs)
        self.num_test_images = len(self.test_xs)

    def load_train_batch(self, batch_size):
        x_out = []
        y_out = []
        z_out = []
        for i in range(0, batch_size):
            hdulnew = fits.open(self.train_xs[(self.train_batch_pointer + i) % self.num_train_images])
            x_out.append(hdulnew[0].data)
            y_out.append([self.train_ys[(self.train_batch_pointer + i) % self.num_train_images]])
            z_out.append([self.train_zs[(self.train_batch_pointer + i) % self.num_train_images]])
            hdulnew.close()
        self.train_batch_pointer += batch_size
        return x_out, y_out, z_out

    def load_test_batch(self, batch_size):
        x_out = []
        y_out = []
        z_out = []
        for i in range(0, self.num_test_images):
            hdulnew = fits.open(self.test_xs[(self.test_batch_pointer + i) % self.num_test_images])
            x_out.append(hdulnew[0].data)
            y_out.append([self.test_ys[(self.test_batch_pointer + i) % self.num_test_images]])
            z_out.append([self.test_zs[(self.test_batch_pointer + i) % self.num_test_images]])
            hdulnew.close()
        self.test_batch_pointer += batch_size
        return x_out, y_out, z_out

def weight_variable(shape):
    initializer = tf.contrib.layers.xavier_initializer_conv2d()
    initial = initializer(shape=shape)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W, stride):
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='VALID')

def pool2d(x, W, stride):
    return tf.nn.max_pool(x, ksize=[1, W, W, 1], strides=[1, stride, stride, 1], padding='VALID')


class ConvModel(object):
    #''' Implements the ConvNet model from the NVIDIA paper '''
    def __init__(self):
        x = tf.placeholder(tf.float32, shape=[None, 8, 8, 3], name='x')
        y_ = tf.placeholder(tf.float32, shape=[None, 1])
        z = tf.placeholder(tf.float32, shape=[None, 1], name='z')
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        # 3 Conv Layers
        W_conv1 = weight_variable([3, 3, 3, 24])
        b_conv1 = bias_variable([24])
        h_conv1 = tf.nn.relu(conv2d(x, W_conv1, 1) + b_conv1)

        W_conv2 = weight_variable([3, 3, 24, 48])
        b_conv2 = bias_variable([48])
        h_conv2 = tf.nn.relu(conv2d(h_conv1, W_conv2, 1) + b_conv2)

        W_conv3 = weight_variable([3, 3, 48, 64])
        b_conv3 = bias_variable([64])
        h_conv3 = tf.nn.relu(conv2d(h_conv2, W_conv3, 1) + b_conv3)
            
        # 3 FC Layers
        W_fc1 = weight_variable([256, 100])
        b_fc1 = bias_variable([100])

        h_conv3_flat = tf.reshape(h_conv3, [-1, 256])
        h_fc1 = tf.nn.relu(tf.matmul(h_conv3_flat, W_fc1) + b_fc1)
        

        W_fc2 = weight_variable([100, 10])
        b_fc2 = bias_variable([10])
        W_z = weight_variable([1, 1])
        h_fc2 = tf.nn.relu(tf.matmul(h_fc1, W_fc2) + tf.matmul(z, W_z) + b_fc2)
        #h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)

        W_fc3 = weight_variable([10, 1])
        b_fc3 = bias_variable([1])
        y = tf.multiply(tf.matmul(h_fc2, W_fc3) + b_fc3, 2, name='y')

        self.x = x
        self.y_ = y_
        self.y = y
        self.z = z
        self.keep_prob = keep_prob


def get_arguments():
    parser = argparse.ArgumentParser(description='ConvNet training')
    parser.add_argument('--batch_size', type=int, default=BATCH_SIZE,
                        help='Number of images in batch.')
    parser.add_argument('--store_metadata', type=bool, default=False,
                        help='Storing debug information for TensorBoard.')
    parser.add_argument('--logdir', type=str, default=LOGDIR,
                        help='Directory for log files.')
    parser.add_argument('--restore_from', type=str, default=None,
                        help='Checkpoint file to restore model weights from.')
    parser.add_argument('--checkpoint_every', type=int, default=CHECKPOINT_EVERY,
                        help='How many steps to save each checkpoint after')
    parser.add_argument('--num_steps', type=int, default=NUM_STEPS,
                        help='Number of training steps.')
    parser.add_argument('--learning_rate', type=float, default=LEARNING_RATE,
                        help='Learning rate for training.')
    parser.add_argument('--keep_prob', type=float, default=KEEP_PROB,
                        help='Dropout keep probability.')
    parser.add_argument('--l2_reg', type=float,
                        default=L2_REG)
    return parser.parse_args()


def main():
    dataset_size = 10

    for idx in range (0, dataset_size):
        args = get_arguments()
        sess = tf.Session()

        model = ConvModel()
        train_vars = tf.trainable_variables()
        global_step = tf.Variable(0, name='global_step', trainable=False)
        loss = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(model.y_, model.y))))
        # + tf.add_n([tf.nn.l2_loss(v) for v in train_vars]) * args.l2_reg
        val = model.y
        learning_rate = tf.train.exponential_decay(args.learning_rate, global_step, decay_steps, decay_rate)
        train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss, global_step)

        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()

        if args.restore_from is not None:
            saver.restore(sess, args.logdir + '/' + args.restore_from)
            start_step = float(args.restore_from.split('step-')[0].split('-')[-1])
            print('Model restored from ' + args.logdir + '/' + args.restore_from)

        if args.store_metadata:
            tf.scalar_summary("loss", loss)
            merged_summary_op = tf.merge_all_summaries()
            summary_writer = tf.train.SummaryWriter(args.logdir, graph=tf.get_default_graph())

        min_loss = 1.0
        data_reader = DataReader(idx)
        train_idx = []
        test_idx = []
        train_error_arr = []
        test_error_arr = []

        while tf.train.global_step(sess, global_step) < args.num_steps + 1:
            i = tf.train.global_step(sess, global_step)
            xs, ys, zs = data_reader.load_train_batch(args.batch_size)
            train_step.run(session=sess, feed_dict={model.x: xs, model.y_: ys, model.z: zs, model.keep_prob: args.keep_prob})
            train_error = 10. * loss.eval(session=sess, feed_dict={model.x: xs, model.y_: ys, model.z: zs, model.keep_prob: 1.0})
            print("Step %d, train loss %g" % (i, train_error))
            if i >= 100:
                train_error_arr.append(train_error)
                train_idx.append(i)

            if i % 25 == 0:
                xs, ys, zs = data_reader.load_test_batch(args.batch_size)
                test_error = 10. * loss.eval(session=sess, feed_dict={model.x: xs, model.y_: ys, model.z: zs, model.keep_prob: 1.0})
                print("Step %d, test loss %g" % (i, test_error))
                if i >= 100:
                    test_error_arr.append(test_error)
                    test_idx.append(i)

                if (i % args.checkpoint_every) == 0 and i > 0:
                    if not os.path.exists(args.logdir):
                        os.makedirs(args.logdir)
                        checkpoint_path = os.path.join(args.logdir, "model-idx-%d-step-%d-test-%g.ckpt" % (idx, i, test_error))
                        filename = saver.save(sess, checkpoint_path)
                        print("Model saved in file: %s" % filename)
                    elif test_error < min_loss:
                        min_loss = test_error
                        if not os.path.exists(args.logdir):
                            os.makedirs(args.logdir)
                        checkpoint_path = os.path.join(args.logdir, "model-idx-%d-step-%d-test-%g.ckpt" % (idx, i, test_error))
                        filename = saver.save(sess, checkpoint_path)
                        print("Model saved in file: %s" % filename)

                        new_fig = plt.figure(dpi=200)
                        y_test = 10. * np.asarray(ys).flatten()
                        y_pred = 10. * val.eval(session=sess, feed_dict={model.x: xs, model.y_: ys, model.z: zs, model.keep_prob: 1.0}).flatten()
                        
                        np.savez('figs/' + 'DR15_' + str(idx) + '_' + str(i) + '_' + str(test_error), y_test, y_pred)
                        heatmap, xedges, yedges = np.histogram2d(y_test, y_pred, bins=250)
                        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
                        plt.imshow(heatmap.T, extent=extent, origin='lower', cmap='tab20')
                        plt.title('Labelled vs Predicted Stellar Mass')
                        plt.xlabel('Labelled Stellar Mass')
                        plt.ylabel('Predicted Stellar Mass')
                        plt.colorbar()

                        xmin, xmax = plt.xlim()
                        ymin, ymax = plt.ylim()
                        limit = (max(xmin, ymin), min(xmax, ymax))
                        diag_line, = plt.plot(limit, limit, ls="--", c=".1")
                        new_fig.savefig('figs/' + 'DR15_' + str(idx) + '_' + str(i) + '_' + str(test_error) + '.png', dpi=new_fig.dpi)
                        plt.close(new_fig)

        fig = plt.figure()
        plt.plot(train_idx, train_error_arr, color='r', label='Training Error')
        plt.plot(test_idx, test_error_arr, color='b', label='Testing Error')
        plt.title('Training & Testing Error')
        plt.xlabel('Iterations')
        plt.ylabel('RMS Error')
        fig.savefig('figs/train' + str(idx) + '.png', dpi=fig.dpi)
        plt.close(fig)


if __name__ == '__main__':
    main()