import os
import tensorflow as tf
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import glob
from astropy.io import fits


def weight_variable(shape):
    initializer = tf.contrib.layers.xavier_initializer_conv2d()
    initial = initializer(shape=shape)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv2d(x, W, stride):
    return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding='VALID')

def pool2d(x, W, stride):
    return tf.nn.max_pool(x, ksize=[1, W, W, 1], strides=[1, stride, stride, 1], padding='VALID')


class ConvModel(object):
    #''' Implements the ConvNet model from the NVIDIA paper '''
    def __init__(self):
        x = tf.placeholder(tf.float32, shape=[None, 8, 8, 3], name='x')
        y_ = tf.placeholder(tf.float32, shape=[None, 1])
        z = tf.placeholder(tf.float32, shape=[None, 1], name='z')
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        # 3 Conv Layers
        W_conv1 = weight_variable([3, 3, 3, 24])
        b_conv1 = bias_variable([24])
        h_conv1 = tf.nn.relu(conv2d(x, W_conv1, 1) + b_conv1)

        W_conv2 = weight_variable([3, 3, 24, 48])
        b_conv2 = bias_variable([48])
        h_conv2 = tf.nn.relu(conv2d(h_conv1, W_conv2, 1) + b_conv2)

        W_conv3 = weight_variable([3, 3, 48, 64])
        b_conv3 = bias_variable([64])
        h_conv3 = tf.nn.relu(conv2d(h_conv2, W_conv3, 1) + b_conv3)
            
        # 3 FC Layers
        W_fc1 = weight_variable([256, 100])
        b_fc1 = bias_variable([100])

        h_conv3_flat = tf.reshape(h_conv3, [-1, 256])
        h_fc1 = tf.nn.relu(tf.matmul(h_conv3_flat, W_fc1) + b_fc1)
        

        W_fc2 = weight_variable([100, 10])
        b_fc2 = bias_variable([10])
        W_z = weight_variable([1, 1])
        h_fc2 = tf.nn.relu(tf.matmul(h_fc1, W_fc2) + tf.matmul(z, W_z) + b_fc2)
        #h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)

        W_fc3 = weight_variable([10, 1])
        b_fc3 = bias_variable([1])
        y = tf.multiply(tf.matmul(h_fc2, W_fc3) + b_fc3, 2, name='y')

        self.x = x
        self.y_ = y_
        self.y = y
        self.z = z
        self.keep_prob = keep_prob

dataset_size = 10
model_list = ['model-step-3000-test-0.459147.ckpt', 'model-step-2850-test-0.459909.ckpt', 'model-step-2950-test-0.48835.ckpt', 'model-step-2850-test-0.467716.ckpt', 'model-step-1200-test-0.496061.ckpt', \
    'model-step-2900-test-0.459766.ckpt', 'model-step-2950-test-0.47041.ckpt', 'model-step-2900-test-0.576356.ckpt', 'model-idx-8-step-3000-test-0.449638.ckpt', 'model-idx-9-step-2650-test-0.492488.ckpt']

for idx in range (0, dataset_size):
    # Load Saver
    sess = tf.Session()
    model = ConvModel()
    train_vars = tf.trainable_variables()
    new_saver = tf.train.Saver()
    #new_saver = tf.train.import_meta_graph('Result/train_model_z/model-step-9800-test-0.000290119.ckpt.meta')
    new_saver.restore(sess, 'train_model/' + model_list[idx])

    #tf.train.latest_checkpoint('Result/train_model_z')
    loss = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(model.y_, model.y))))
    val = model.y

    # Load validation data
    y_pred = []
    y_golden = []
    ra = []
    dec = []
    z = []
    sersic = []

    total_count = 0
    for file in glob.glob("reserved_DR15_" + str(idx) + "/fits/*.fits"):
        print('Inferencing Galaxy Number: ' + str(total_count))
        hdulnew = fits.open(file)
        count = 0
        xs = []
        ys = []
        zs = []

        for idx_chunk in range(0, hdulnew[0].data.shape[0]):
            xs.append(hdulnew[0].data[idx_chunk, :, :, :])
            ys.append(0)
            zs.append(hdulnew[0].header['Z'])
            count += 1

        hdulnew.close()
        print('Number of Chunks: ' + str(count))

        xnew = np.array(xs).reshape(count, 8, 8, 3)
        ynew = np.array(ys).reshape(count, 1)
        znew = np.array(zs).reshape(count, 1)

        y_out = 10. * val.eval(session=sess, feed_dict={model.x: xnew, model.y_: ynew, model.z: znew, model.keep_prob: 1.0}).flatten()

        if np.log10(np.sum(10 ** np.array(y_out))) < 13:
            y_pred.append(np.log10(np.sum(10 ** np.array(y_out))))
            y_golden.append(hdulnew[0].header['STELLAR_MASS'])
            ra.append(hdulnew[0].header['RA'])
            dec.append(hdulnew[0].header['DEC'])
            z.append(hdulnew[0].header['Z'])
            sersic.append(hdulnew[0].header['SERSIC_MASS'])
            total_count += 1

        print(" ")

        
    print('Loaded ' + str(total_count) + ' galaxies')

    y_golden = np.asarray(y_golden).flatten()
    y_pred = np.asarray(y_pred).flatten()
    y_pred = y_pred
    rmse = np.sqrt((np.square(y_golden - y_pred)).mean(axis=0))

    new_fig = plt.figure()
    print('Final test error: %g' % (rmse))
    np.savez('figs/' + 'test_full_' + str(idx) + '_' + str(rmse), gold=y_golden, pred=y_pred, ra=ra, dec=dec, z=z, sersic=sersic)



    heatmap, xedges, yedges = np.histogram2d(y_golden, y_pred, bins=150)
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    #extent = [xedges[0], 12, yedges[0], 12]
    plt.imshow(heatmap.T, extent=extent, origin='lower', cmap='tab20')
    plt.title('Labelled vs Predicted Stellar Mass')
    plt.xlabel('Labelled Stellar Mass')
    plt.ylabel('Predicted Stellar Mass')
    plt.colorbar()


    ax = plt.gca()
    # xticks = [7, 8, 9, 10, 11, 12]
    # yticks = [7, 8, 9, 10, 11, 12]
    # ax.set_xticks(xticks)
    # ax.set_yticks(yticks)

    xmin, xmax = plt.xlim()
    ymin, ymax = plt.ylim()
    limit = (max(xmin, ymin), min(xmax, ymax))
    diag_line, = plt.plot(limit, limit, ls="--", c=".1")
    new_fig.savefig('figs/' + 'test_' + str(idx) + '_' + str(rmse) + '.png', dpi=1000)
    plt.close(new_fig)