# To reduce SDSS image data into 10*10 chunks with Voronoi stellar mass data for DR15

from astropy.io import fits
from astropy.table import Table
import numpy as np
from astropy import units as u
from matplotlib import pyplot as plt
from astroquery.sdss import SDSS
from astropy import coordinates as coords
from astropy import wcs
import glob
import os.path
from scipy.spatial import Voronoi
from scipy.spatial import KDTree
from collections import Counter

# Load FITS files and set variables
hdulInfo = fits.open('manga_firefly-v2_4_3-STELLARPOP.fits')
hdulLink = fits.open('MaNGA_targets_extNSA_tiled_ancillary.fits')
hduNSA = fits.open('nsa_v1_0_1.fits')

Coordata = hdulInfo[1].data
Regiondata = hdulInfo[4].data
Massdata = hdulInfo[11].data
Linkdata = hdulLink[1].data
NSA_data = hduNSA[1].data

galaxy_count = 4675
count = 0
size = 8
step = size * 0.396
pixel_rad = 76
bands = ['g', 'r', 'i']

dataset_size = 10

for dataset_idx in range(dataset_size):
	for galaxy_idx in range(galaxy_count):
		# Load astronomical parameters
		manga_id = Coordata[galaxy_idx][0]
		ra = Coordata[galaxy_idx][4]
		dec = Coordata[galaxy_idx][5]
		z = Coordata[galaxy_idx][6]

		# Check if galaxy is suitable
		nsa_id = Linkdata[Linkdata['MANGAID'] == manga_id]
		if nsa_id.shape[0] != 1:
			continue
		query = NSA_data[NSA_data['NSAID'] == nsa_id['NSA_NSAID'][0]]
		if query.shape[0] != 1:
			continue

		file = "DR15_2MASS_fits/" + manga_id + ".fits"
		if not os.path.isfile(file):
			print("No DATA")
			print(" ")
			continue

		file = "DR15_data/frame-r-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
		if not os.path.isfile(file):
			print("No DATA")
			print(" ")
			continue
		file = "DR15_data/frame-i-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
		if not os.path.isfile(file):
			print("No DATA")
			print(" ")
			continue
		file = "DR15_data/frame-g-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
		if not os.path.isfile(file):
			print("No DATA")
			print(" ")
			continue

		num_points = 2800
		for i in range (0, Regiondata.shape[1]):
			if (Regiondata[galaxy_idx,i,1] == -9999.0):
				num_points = i
				break
		print("num_points: " + str(num_points))

		if (num_points == 0):
			print("No stellar mass data")
			continue

		masses = Massdata[galaxy_idx,:num_points,0]
		mass = float(0.0)
		total_mass = np.log10(np.sum(10 ** np.array(masses)))
		if total_mass == float("-inf"):
			print("Invalid Mass")
			continue
		sersic_mass = nsa_id['NSA_SERSIC_MASS'][0]

		count += 1
		print("Processing galaxy id: " + str(manga_id))
		print("Galaxy count: " + str(galaxy_idx))
		print("Total count: " + str(count))

		# Set radius and image containers
		radius = query['PETRO_TH90'][0] * 2
		far = int(np.floor((pixel_rad - size / 2) / size))

		pics = []
		pic = np.zeros((size, size, 3), dtype=np.float32)

		hdulphoto = fits.open(file)
		im = np.zeros((hdulphoto[0].data.shape[0], hdulphoto[0].data.shape[1], 3), dtype=np.float32)
		hd_new = hdulphoto[0].header
		hdulphoto.close()

		# Calculate pixelized stellar mass
		file = "DR15_2MASS_fits/" + manga_id + ".fits"
		hdultemp = fits.open(file)
		hd_old = hdultemp[0].header
		hdultemp.close()

		band_count = 0
		for band in bands:
			file = "DR15_data/frame-" + band + "-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
			hdulphoto = fits.open(file)
			im[:,:,band_count] = hdulphoto[0].data
			hdulphoto.close()
			band_count += 1

		coors = np.zeros(Regiondata[galaxy_idx,:num_points,1:3].shape)

		w = wcs.WCS(hd_old)
		world = np.array([[ra, dec]], np.float_)
		obj_pixel = w.wcs_world2pix(world, 1)
		print(obj_pixel)
		coors[:,0] = Regiondata[galaxy_idx,:num_points,2] + obj_pixel[0, 0]
		coors[:,1] = Regiondata[galaxy_idx,:num_points,1] + obj_pixel[0, 1]
		ra_dec = w.wcs_pix2world(coors, 1)

		w = wcs.WCS(hd_new)
		mark_pixel = w.wcs_world2pix(ra_dec, 1)
		world = np.array([[ra, dec]], np.float_)
		obj_pixel = w.wcs_world2pix(world, 1)
		x = -(mark_pixel[:,1] - obj_pixel[0, 1])
		y = mark_pixel[:,0] - obj_pixel[0, 0]

		vac_points = np.empty([num_points, 2])
		vac_points[:,0] = x
		vac_points[:,1] = y

		tree = KDTree(vac_points)

		x_idx = np.linspace(-pixel_rad, pixel_rad - 1, 2 * pixel_rad)
		y_idx = np.linspace(-pixel_rad, pixel_rad - 1, 2 * pixel_rad)
		xx, yy = np.meshgrid(x_idx, y_idx)
		xy = np.c_[xx.ravel(), yy.ravel()]

		data_idx = tree.query(xy)[1]
		count_map = Counter(data_idx)
		data_idx = data_idx.reshape(2 * pixel_rad, 2 * pixel_rad)
		data_LAGE = masses[data_idx]

		# Generate reference image files
		combined_mass = 0.0
		obj_pixel = np.zeros((3, 2), dtype=np.float64)
		prob = np.random.rand()
		band_count = 0
		for band in bands:
			file = "DR15_data/frame-" + band + "-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
			hdulphoto = fits.open(file)
			im[:,:,band_count] = hdulphoto[0].data
			hd_new = hdulphoto[0].header
			hdulphoto.close()
		
			w = wcs.WCS(hd_new)
			world = np.array([[ra, dec]], np.float_)
			obj_pixel[band_count,:] = w.wcs_world2pix(world, 1)
			print('Object Pixel Indices: ')
			print(obj_pixel[band_count,:])

			if (obj_pixel[band_count, 1] - (far+1)*size >= 0 and obj_pixel[band_count, 1] + (far+1)*size < im.shape[0] and obj_pixel[band_count, 0] - (far+1)*size >= 0 and obj_pixel[band_count, 0] + (far+1)*size < im.shape[1]):
				new_fig = plt.figure()
				pic2 = im[int(obj_pixel[band_count, 1] - pixel_rad):int(obj_pixel[band_count, 1] + pixel_rad), int(obj_pixel[band_count, 0] - pixel_rad):int(obj_pixel[band_count, 0] + pixel_rad), band_count]
				implot = plt.imshow(pic2, extent=[-pic2.shape[1]/2., pic2.shape[1]/2., -pic2.shape[0]/2., pic2.shape[0]/2.])
				plt.xlabel("x(pixel)")
				plt.ylabel("y(pixel)")
				plt.title(manga_id)
				if prob < 0.8:
					new_fig.savefig('MaNGA_DR15_' + str(dataset_idx) + '/refs/' + str(manga_id) + '_photometry_' + band + '.png', dpi=new_fig.dpi)
				else:
					new_fig.savefig('reserved_DR15_' + str(dataset_idx) + '/refs/' + str(manga_id) + '_photometry_' + band + '.png', dpi=new_fig.dpi)
				plt.close(new_fig)
			else:
				print("Out of Bounds!!!!!!")
				break
			band_count += 1

		if band_count != 3:
			continue

		# Iterate for all the chunks with center point within the radius
		for i in range (-far, far + 1):
			for j in range (-far, far + 1):
				closest_point = vac_points[data_idx[pixel_rad + size*i][pixel_rad + size*j], :]
				if ((closest_point[1] - size*i) ** 2 + (closest_point[0] - size*j) ** 2 <= (size/2) ** 2):
					band_idx = 0
					for band in bands:
						pic[:,:,band_idx] = im[int(obj_pixel[band_idx, 1] + size*i - size/2):int(obj_pixel[band_idx, 1] + size*i + size/2), int(obj_pixel[band_idx, 0] + size*j - size/2):int(obj_pixel[band_idx, 0] + size*j + size/2), band_idx]
						band_idx += 1
					pics.append(pic.copy())
					if prob < 0.8:
						plt.imsave('MaNGA_DR15_' + str(dataset_idx) + '/images/' + str(manga_id) + '_photometry_' + str(i) + '_' + str(j) + '.png', pics[-1][:,:,0])
					else:
						plt.imsave('reserved_DR15_' + str(dataset_idx) + '/images/' + str(manga_id) + '_photometry_' + str(i) + '_' + str(j) + '.png', pics[-1][:,:,0])

					# Normalize stellar mass w.r.t area
					idx_pic = data_idx[int(pixel_rad + size*i - size/2):int(pixel_rad + size*i + size/2), int(pixel_rad + size*j - size/2):int(pixel_rad + size*j + size/2)]
					mass_pic = data_LAGE[int(pixel_rad + size*i - size/2):int(pixel_rad + size*i + size/2), int(pixel_rad + size*j - size/2):int(pixel_rad + size*j + size/2)]
					# print(idx_pic)
					count_map_pic = Counter(idx_pic.flatten()).most_common()
					for k in range (0, len(count_map_pic)):
						block_idx = count_map_pic[k][0]
						idx_mass = (10 ** masses[block_idx]) * float(count_map_pic[k][1]) / float(count_map[block_idx])
						mass += idx_mass
						# print(block_idx)
						# print(idx_mass)
					combined_mass += mass
					mass = np.log10(mass)
					# print('x and y indices: ' + str(i) + ' ' + str(j) + '  mass: ' + str(mass))
					# print(" ")

					hdu = fits.PrimaryHDU(pic)
					hdr = hdu.header
					hdr['Z'] = z
					hdr['STELLAR_MASS'] = mass

					if prob < 0.8:
						if not os.path.isfile('MaNGA_DR15_' + str(dataset_idx) + '/fits/' + str(manga_id) + '_' + str(i) + '_' + str(j) + '.fits'):
							hdu.writeto('MaNGA_DR15_' + str(dataset_idx) + '/fits/' + str(manga_id) + '_' + str(i) + '_' + str(j) + '.fits')
							plt.imsave('MaNGA_DR15_' + str(dataset_idx) + '/masses/' + str(manga_id) + '_mass_' + str(i) + '_' + str(j) + '.png', mass_pic)
					else:
						if not os.path.isfile('reserved_DR15_' + str(dataset_idx) + '/masses/' + str(manga_id) + '_mass_' + str(i) + '_' + str(j) + '.png'):
							plt.imsave('reserved_DR15_' + str(dataset_idx) + '/masses/' + str(manga_id) + '_mass_' + str(i) + '_' + str(j) + '.png', mass_pic)


		if not pics:
			print("No valid image blocks")
			continue
		pic_result = np.concatenate([arr[np.newaxis] for arr in pics])
		print(pic_result.shape)
		hdu = fits.PrimaryHDU(pic_result)
		hdr = hdu.header
		hdr['RA'] = ra
		hdr['DEC'] = dec
		hdr['Z'] = z
		hdr['STELLAR_MASS'] = total_mass
		hdr['SERSIC_MASS'] = sersic_mass
		print("Total Mass: " + str(total_mass))
		print("Combined Mass:" + str(np.log10(combined_mass)))

		if prob > 0.8:
			if not os.path.isfile('reserved_DR15_' + str(dataset_idx) + '/fits/' + str(manga_id) + '.fits'):
				hdu.writeto('reserved_DR15_' + str(dataset_idx) + '/fits/' + str(manga_id) + '.fits')

		print(" ")
