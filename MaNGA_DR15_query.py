# Query SDSS DR12 photometry for DR15 MaNGA Objects

from astropy.io import fits
from astropy.table import Table
import numpy as np
from astropy import units as u
from matplotlib import pyplot as plt
from astroquery.sdss import SDSS
from astropy import coordinates as coords
import urllib.request

hdulInfo = fits.open('manga_firefly-v2_4_3-STELLARPOP.fits')
hdulLink = fits.open('MaNGA_targets_extNSA_tiled_ancillary.fits')
hduNSA = fits.open('nsa_v1_0_1.fits')

Coordata = hdulInfo[1].data
Regiondata = hdulInfo[4].data
Massdata = hdulInfo[11].data
Linkdata = hdulLink[1].data
NSA_data = hduNSA[1].data


#galaxy_count = 5
galaxy_count = 4675
count = 0
bands = ['g', 'r', 'i']
size = 100

for galaxy_idx in range(galaxy_count):
    manga_id = Coordata[galaxy_idx][0]
    ra = Coordata[galaxy_idx][4]
    dec = Coordata[galaxy_idx][5]
    z = Coordata[galaxy_idx][6]

    nsa_id = Linkdata[Linkdata['MANGAID'] == manga_id]
    if nsa_id.shape[0] == 0:
        print("No MaNGA target in list")
        continue
    query = NSA_data[NSA_data['IAUNAME'] == ''.join(nsa_id['NSA_IAUNAME'][0])]

    if query.shape[0] != 1:
        print("No Matching IAUNAME")

    if query.shape[0] == 1:
        if abs(ra - np.float32(query['RA'][0])) < 0.0001 and abs(dec - np.float32(query['DEC'][0])) < 0.0001:
            print('Coordinate Aligns for galaxy index: ' + str(galaxy_idx) + ' count:' + str(count))
        else:
            print("Coordinate does not match")
            continue
        count += 1

        for band in bands:
            url = "http://data.sdss3.org/sas/dr12/boss/photoObj/frames/301/" + str(query['RUN'][0]) + "/" + str(query['CAMCOL'][0]) + "/frame-" + band + "-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
            file = "new_data/frame-" + band + "-" + "{0:0=6d}".format(query['RUN'][0]) + "-" + str(query['CAMCOL'][0]) + "-" + "{0:0=4d}".format(query['FIELD'][0]) + ".fits.bz2"
            print("Accessing: " + url)

            urllib.request.urlretrieve(url, file)
            # pos = coords.SkyCoord(ra * u.deg, dec * u.deg, frame='icrs')
            # xid = SDSS.query_region(pos, spectro=True)
            #result = SDSS.get_images(matches=xid, band='g')

            result = fits.open(file)
            im = result[0].data
            hd = result[0].header
            print('Image Size: ', im.shape)

            trans_mat = np.matrix([[hd['CD1_1'], hd['CD2_1']], [hd['CD1_2'], hd['CD2_2']]])
            coor_diff = np.matrix([ra - hd['CRVAL1'], dec - hd['CRVAL2']])
            print('Object Coordinate Diff: ', coor_diff)

            pixel_diff = coor_diff * np.linalg.inv(trans_mat)
            obj_pixel = np.around(np.matrix([hd['CRPIX1'], hd['CRPIX2']]) + pixel_diff)
            print('Object Pixel Indices: ', obj_pixel)
            print(" ")

            if (obj_pixel[0, 1] < size/2 or obj_pixel[0, 1] >= im.shape[0] - size/2 
                or obj_pixel[0, 0] < size/2 or obj_pixel[0, 0] >= im.shape[1] - size/2):
                print('Object Pixel Out of Bounds!!!')
                print(" ")
                continue

            print(" ")
            
            plt.imsave('new_images/' + ''.join(manga_id) + '_' + band + '.png', im[int(obj_pixel[0, 1] - size/2):int(obj_pixel[0, 1] + size/2), 
                     int(obj_pixel[0, 0] - size/2):int(obj_pixel[0, 0] + size/2)])

print("Total number of galaxies: " + str(count))